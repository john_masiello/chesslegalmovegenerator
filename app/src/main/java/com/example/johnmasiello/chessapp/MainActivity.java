package com.example.johnmasiello.chessapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);

        // Create the text view
        TextView textView = new TextView(this);
        textView.setTextSize(40);

        // put the result of my chess program into textView
        textView.setText(MainChessApp.getString());

        // Set the text view as the activity layout
        setContentView(textView);
    }
}
